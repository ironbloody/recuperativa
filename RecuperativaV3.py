import json

# funcion para cargar el json


def cargar():
    with open("iris.json", 'r') as file:
        dic = json.load(file)
    return dic


def guardar_setosa(dic):
    with open("setosa.json", 'w') as file:
        json.dump(dic, file)


# Funcion que me mostrara las especies de las flores


def mostrar():
    dic = cargar()
    '''diccionario por comprension que me eliminara
    los elementos repetidos para luego imprimiar las especies sin problemas'''
    repetidos = {x['species']: x for x in dic}.values()
    print("---------Especies---------")
    for i in repetidos:
            print('Especie:', i['species'])
    print("--------------------------")

# Funcion que calcula promedio de setosa


def promedio_setosa():
    dic = cargar()
    sumasetosa1 = 0
    sumasetosa2 = 0
    for i in dic:
        # Condicion para que solo tome en cuenta las setosas
        if i["species"] == "setosa":
            petallength = (i["petalLength"])
            petalwidth = (i["petalWidth"])
            sumasetosa1 += petallength
            sumasetosa2 += petalwidth
    # Format que par disminuir la cantidad de decimal
    promediosetosa1 = "{0:.2f}".format(sumasetosa1/flores)
    promediosetosa2 = "{0:.2f}".format(sumasetosa2/flores)
    print("Promedio largo petalos setosa: ", promediosetosa1)
    print("Promedio ancho petalos setosa: ", promediosetosa2)


# Funcion que calcula promedio de versicolor


def promedio_versicolor():
    dic = cargar()
    sumaversicolor1 = 0
    sumaversicolor2 = 0
    for i in dic:
        # Condicion para que solo tome en cuenta las versicolor
        if i["species"] == "versicolor":
            petallength = (i["petalLength"])
            petalwidth = (i["petalWidth"])
            sumaversicolor1 += petallength
            sumaversicolor2 += petalwidth
    # Format que par disminuir la cantidad de decimal
    promedioversicolor1 = "{0:.2f}".format(sumaversicolor1/flores)
    promedioversicolor2 = "{0:.2f}".format(sumaversicolor2/flores)
    print("Promedio largo petalos versicolor: ", promedioversicolor1)
    print("Promedio ancho petalos versicolor: ", promedioversicolor2)


# Funcion que calcula promedio de virginica


def promedio_virginica():
    dic = cargar()
    sumavirginica1 = 0
    sumavirginica2 = 0
    for i in dic:
        # Condicion para que solo tome en cuenta las virginicas
        if i["species"] == "virginica":
            petallength = (i["petalLength"])
            petalwidth = (i["petalWidth"])
            sumavirginica1 += petallength
            sumavirginica2 += petalwidth
    # Format que par disminuir la cantidad de decimal
    promediovirginica1 = "{0:.2f}".format(sumavirginica1/flores)
    promediovirginica2 = "{0:.2f}".format(sumavirginica2/flores)
    print("Promedio largo petalos virginica: ", promediovirginica1)
    print("Promedio ancho petalos virginica: ", promediovirginica2)


def promedio_petalos(promediosetosa1, promediosetosa2, promedioversicolor1, promediovercolor2, promediovirginica1, promediovirginica2):
    dic = cargar()
    for i in dic:
        if promediosetosa1 and promediosetosa2 >  promedioversicolor1 and promediovercolor2 or promediovirginica1 and promediovirginica2:
            print("Especie con promedio mas alto es: ", i["species"])


def medida_maxima():
    dic = cargar()
    for i in dic:
        petallength = (i["sepalLength"])
        print(petallength)


def pasar_setosa():
    dic = cargar()
    a = {}
    for i in dic:
        if i["species"] == "setosa":
            print(i)

# Funcion main que llama a todas las funciones


if __name__ == "__main__":
    # Pequeño calculo para tomar la cantidad de flores
    dic = cargar()
    especies = []

    for i in dic:
        x = i["species"]
        especies.append(x)

    flores = 0
    for i in especies:
        if i == "setosa":
            flores += 1
    mostrar()
    print("Promedio setosa:")
    promedio_setosa()
    print("Promedio versicolor:")
    promedio_versicolor()
    print("Promedio virginica:")
    promedio_virginica()
    # medida_maxima()
    pasar_setosa()
