import json

# funcion para cargar el json


def cargar():
    with open("iris.json", 'r') as file:
        dic = json.load(file)
    return dic

# funcion que guarda setosa en un json


def guardar_setosa(dic):
    with open("setosa.json", 'w') as file:
        json.dump(dic, file)

# funcion que guarda versicolor en un json


def guardar_versicolor(dic):
    with open("versicolor.json", 'w') as file:
        json.dump(dic, file)

# funcion que guarda virginica en un json


def guardar_virginica(dic):
    with open("virginica.json", 'w') as file:
        json.dump(dic, file)

# Funcion que me mostrara las especies de las flores


def mostrar():
    dic = cargar()
    '''diccionario por comprension que me eliminara
    los elementos repetidos para luego imprimiar las especies sin problemas'''
    repetidos = {x['species']: x for x in dic}.values()
    print("---------Especies---------")
    for i in repetidos:
            print('Especie:', i['species'])
    print("--------------------------")

# Funcion que calcula promedio de todas las flores


def promedios():
    # calculo promedio setosa
    print("Promedio setosa: ")
    dic = cargar()
    sumasetosa1 = 0
    sumasetosa2 = 0
    especies = []
    for i in dic:
        x = i["species"]
        especies.append(x)
    flores = 0
    for i in especies:
        if i == "setosa":
            flores += 1
    for i in dic:
        if i["species"] == "setosa":
            petallength = (i["petalLength"])
            petalwidth = (i["petalWidth"])
            sumasetosa1 += petallength
            sumasetosa2 += petalwidth
    # Format para disminuir la cantidad de decimal
    promediosetosa1 = "{0:.2f}".format(sumasetosa1 / flores)
    promediosetosa2 = "{0:.2f}".format(sumasetosa2 / flores)
    print("Promedio largo petalos setosa: ", promediosetosa1)
    print("Promedio ancho petalos setosa: ", promediosetosa2)

    print("\nPromedio versicolor: ")
    dic = cargar()
    sumaversicolor1 = 0
    sumaversicolor2 = 0
    especies = []
    for i in dic:
        x = i["species"]
        especies.append(x)
    flores = 0
    for i in especies:
        if i == "versicolor":
            flores += 1
    for i in dic:
        if i["species"] == "versicolor":
            petallength = (i["petalLength"])
            petalwidth = (i["petalWidth"])
            sumaversicolor1 += petallength
            sumaversicolor2 += petalwidth
    # Format que par disminuir la cantidad de decimal
    promedioversicolor1 = "{0:.2f}".format(sumaversicolor1 / flores)
    promedioversicolor2 = "{0:.2f}".format(sumaversicolor2 / flores)
    print("Promedio largo petalos versicolor: ", promedioversicolor1)
    print("Promedio ancho petalos versicolor: ", promedioversicolor2)

    print("\nPromedio virginica: ")
    dic = cargar()
    sumavirginica1 = 0
    sumavirginica2 = 0
    especies = []
    for i in dic:
        x = i["species"]
        especies.append(x)
    flores = 0
    for i in especies:
        if i == "versicolor":
            flores += 1
    for i in dic:
        if i["species"] == "virginica":
            petallength = (i["petalLength"])
            petalwidth = (i["petalWidth"])
            sumavirginica1 += petallength
            sumavirginica2 += petalwidth
    # Format que par disminuir la cantidad de decimal
    promediovirginica1 = "{0:.2f}".format(sumavirginica1 / flores)
    promediovirginica2 = "{0:.2f}".format(sumavirginica2 / flores)
    print("Promedio largo petalos virginica: ", promediovirginica1)
    print("Promedio ancho petalos virginica: ", promediovirginica2)

    # calculo del petalo mayor
    petalo_largo = 0
    if promediosetosa1 > promedioversicolor1:
        petalo_largo = promediosetosa1
        if promediovirginica1 > petalo_largo:
            petalo_largo = promediovirginica1
            print("\npetalo mayor: virginica:", petalo_largo)
        else:
            print("\npetalo mayor: setosa:", petalo_largo)
    elif promediosetosa1 < promedioversicolor1:
        petalo_largo = promedioversicolor1
        if promediovirginica1 > petalo_largo:
            petalo_largo = promediovirginica1
            print("\npetalo mayor: virginica:", petalo_largo)
        else:
            print("\npetalo mayor: versicolor:", petalo_largo)
# Funcion que me calcula la medida maxima


def medida_maxima():
    dic = cargar()
    medidamaxima = []
    repetidos = []
    for i in dic:
        x = i["petalWidth"]
        medidamaxima.append(x)
    '''relleno una lista con las flores que tengan
    la medida maxima para poder eliminar las repetidas'''
    for i in dic:
        if i["petalWidth"] == max(medidamaxima):
            repetidos.append(i["species"])

    print("La especie con la medida mas alta es: ", " ".join(set(repetidos)))


'''funcion que me creara una lista que contenga
todas las setosas y las pasara a json'''


def pasar_setosa():
    setosa = []
    dic = cargar()
    for i in dic:
        if i["species"] == "setosa":
            setosa.append(i)
    guardar_setosa(setosa)

'''funcion que me creara una lista que contenga
todas las versicolor y las pasara a json'''


def pasar_versicolor():
    versicolor = []
    dic = cargar()
    for i in dic:
        if i["species"] == "versicolor":
            versicolor.append(i)
    guardar_versicolor(versicolor)

'''funcion que me creara una lista que contenga
todas las virginica y las pasara a json'''


def pasar_virginica():
    virginica = []
    dic = cargar()
    for i in dic:
        if i["species"] == "virginica":
            virginica.append(i)
    guardar_virginica(virginica)


# Funcion main que llama a todas las funciones


if __name__ == "__main__":
    # Pequeño calculo para tomar la cantidad de flores
    dic = cargar()
    especies = []

    for i in dic:
        x = i["species"]
        especies.append(x)

    flores = 0
    for i in especies:
        if i == "setosa":
            flores += 1
    mostrar()
    promedios()
    print("\nMedida maxima: ")
    medida_maxima()
    pasar_setosa()
    pasar_versicolor()
    pasar_virginica()
