import json


# funcion para cargar el json


def cargar():
    with open("iris.json", 'r') as file:
        dic = json.load(file)
    return dic

# Funcion que me mostrara las especies de las flores


def mostrar():
    dic = cargar()
    '''diccionario por comprension que me eliminara
    los elementos repetidos para luego imprimiar las especies sin problemas'''
    repetidos = {x['species']: x for x in dic}.values()
    for i in repetidos:
            print('Especie: ', i['species'])

mostrar()
