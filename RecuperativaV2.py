import json

# funcion para cargar el json


def cargar():
    with open("iris.json", 'r') as file:
        dic = json.load(file)
    return dic

# Funcion que me mostrara las especies de las flores


def mostrar():
    dic = cargar()
    '''diccionario por comprension que me eliminara
    los elementos repetidos para luego imprimiar las especies sin problemas'''
    repetidos = {x['species']: x for x in dic}.values()
    print("---------Especies---------")
    for i in repetidos:
            print('Especie:', i['species'])
    print("--------------------------")


def promedio_setosa():
    dic = cargar()
    sumasetosa1 = 0
    sumasetosa2 = 0
    for i in dic:
        if i["species"] == "setosa":
            petallength = (i["petalLength"])
            petalwidth = (i["petalWidth"])
            sumasetosa1 += petallength
            sumasetosa2 += petalwidth
    promediosetosa1 = sumasetosa1/49
    promediosetosa2 = sumasetosa2/49
    print("Promedio largo petalos setosa: ", promediosetosa1)
    print("Promedio ancho petalos setosa: ", promediosetosa2)


def promedio_versicolor():
    dic = cargar()
    sumaversicolor1 = 0
    sumaversicolor2 = 0
    for i in dic:
        if i["species"] == "versicolor":
            petallength = (i["petalLength"])
            petalwidth = (i["petalWidth"])
            sumaversicolor1 += petallength
            sumaversicolor2 += petalwidth
    promedioversicolor1 = sumaversicolor1/49
    promedioversicolor2 = sumaversicolor2/49
    print("Promedio largo petalos versicolor: ", promedioversicolor1)
    print("Promedio ancho petalos versicolor: ", promedioversicolor2)


def promedio_virginica():
    dic = cargar()
    sumavirginica1 = 0
    sumavirginica2 = 0
    for i in dic:
        if i["species"] == "virginica":
            petallength = (i["petalLength"])
            petalwidth = (i["petalWidth"])
            sumavirginica1 += petallength
            sumavirginica2 += petalwidth
    promediovirginica1 = sumavirginica1/49
    promediovirginica2 = sumavirginica2/49
    print("Promedio largo petalos virginica: ", promediovirginica1)
    print("Promedio ancho petalos virginica: ", promediovirginica2)


def medida_maxima():
    dic = cargar()
    for i in dic:
        str(i["petalLength"])


mostrar()
print("Promedio setosa:")
promedio_setosa()
print("Promedio versicolor:")
promedio_versicolor()
print("Promedio virginica:")
promedio_virginica()
medida_maxima()
